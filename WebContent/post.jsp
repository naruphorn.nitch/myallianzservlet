<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style>
.header {
  font-family:Verdana, Geneva, sans-serif;
}
.button {
  font-family:Verdana, Geneva, sans-serif;
  cursor:pointer;
}
</style>
<h2 class="header" style="text-align:center;" font-family:Verdana, Geneva, sans-serif; > AMOS-MyAllianz </h2>
<input type = "button" class="button" value = "Refresh" style="float: right;" onclick=history.go(0)>
</head>
<body>

<style type="text/css">

	table.table-style-three {
		height:50%;
		width:100%;
		border-collapse:separate;
   		border:2px solid white;
    	border-radius:6px;
    	-moz-border-radius:6px;
		font-family: verdana, arial, sans-serif;
		font-size: 11px;
		border:2px solid white;
		border-color: white;
		border-width: 1px;
	}
	table.table-style-three th {
		border-width: 1px;
		border-color: white;
		border-radius:6px;
    	-moz-border-radius:6px;
		padding: 8px;
		border-style: solid;
		background-color: #d6d6d6;
	}
	table.table-style-three tr:hover td {
		
	}
	table.table-style-three tr:nth-child(even) td{
		background-color: #efefef;
	}
	table.table-style-three td:hover{
		border-width: 1px;
		border-style: solid;
		border-color: white;
		border-radius:6px;
    	-moz-border-radius:6px;
		padding: 8px;
		background-color: #bababa;
	}
</style>
<!-- Table goes in the document BODY -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<table class="table-style-three">
	<thead>
	<tr>
		<th>NAME</th>
		<th>URL</th>
		<th>RESULT</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>01-Login</td>
		<td>${url.get(0)}</td>
		<th style="background-color:${color0}" align="center">${result0}</th>
	</tr>
	<tr>
		<td>02-mypolicy</td>
		<td>${url.get(1)}</td>
		<th style="background-color:${color1}" align="center">${result1}</th>
	</tr>
	<tr>
		<td>03-myclaimusage</td>
		<td>${url.get(2)}</td>
		<th style="background-color:${color2}" align="center">${result2}</th>
	</tr>
	<tr>
		<td>04-notification</td>
		<td>${url.get(3)}</td>
		<th style="background-color:${color3}" align="center">${result3}</th>
	</tr>
	<tr>
		<td>05-myclaimhistory</td>
		<td>${url.get(4)}</td>
		<th style="background-color:${color4}" align="center">${result4}</th>
	</tr>
	<tr>
		<td>azPaymentStatus</td>
		<td>${url.get(5)}</td>
		<th style="background-color:${color5}" align="center">${result5}</th>
	</tr>
	</tbody>
</table>
</body>
</html>