package com.pros.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Url {
	
	public static final List<String> urlList = Collections.unmodifiableList(
		    new ArrayList<String>() {{
		        add("http://e-toyz.azay.co.th/AZauth/api/login");
		        add("http://e-toyz.azay.co.th/AZauth/api/mypolicy");
		        add("http://e-toyz.azay.co.th/AZauth/api/myclaimusage");
		        add("http://e-toyz.azay.co.th/AZauth/api/notification");
		        add("http://e-toyz.azay.co.th/AZauth/api/myclaimhistory");
		        add("http://e-toyz.azay.co.th/AZauth/azayGW/azPaymentStatus");
		        // etc
		    }});
	
}
