package com.pros.controller;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class ExampleServlet
 */
//@WebServlet("/MyAllianzServlet")
public class MyAllianzServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyAllianzServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		SendHttp send = new SendHttp();
		Url url = new Url();
		
		//send.addResult();
		request.setAttribute("url", url.urlList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("post.jsp");
		
		send.addResult(send.setHeaderLogin());
		send.addResult(send.setHeaderMyPolicy());
		send.addResult(send.setHeaderClaim());
		send.addResult(send.setHeaderNoti());
		send.addResult(send.setHeaderClaimHis());
		send.addResult(send.setAzPaymentStatus());

		for(int i = 0; i<send.getResult().size(); i++) {
			if(send.getResult().get(i).equals("PASS")) {
				request.setAttribute("result"+i, "PASS");
				request.setAttribute("color"+i, "#00FF00");
			}
			else {
				request.setAttribute("result"+i, "NOT PASS");
				request.setAttribute("color"+i, "#ff0d00");
			}
		}
		
		dispatcher.forward( request, response );
		
	}

}
