package com.pros.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;




public class SendHttp {

	private String token = "";
	private List<String> result = new ArrayList<>();

	// 01-Login
	public String setHeaderLogin() {
		String name = "01-Login";
		List<Header> header = new ArrayList<Header>();
		header.add(createHeader("code", "3100500776711"));
		header.add(createHeader("Content-Type", "application/json"));
		header.add(createHeader("Accept", "application/json, text/plain, */*"));
		header.add(createHeader("version_no", "1.1.1"));
		header.add(createHeader("client_secret", "a3f63ee0f0fc44848eeef91109396295"));
		header.add(createHeader("client_id", "MYALLIANZ"));

		JsonDetail jsonDet = new JsonDetail();
		Url url = new Url();
		jsonDet = createJsonDetail(url.urlList.get(0),
				"{" + "\"username\" : \"3100500776711\"," + "\"password\" : \"aaaa1111\"" + "}", header);
		return getPostRequest(name, jsonDet);
	}

	// 02-mypolicy
	public String setHeaderMyPolicy() {
		String name = "02-mypolicy";
		List<Header> header = new ArrayList<Header>();
		header.add(createHeader("code", "3100500776711"));
		header.add(createHeader("Accept", "application/json, text/plain, */*"));
		header.add(createHeader("version_no", "1.1"));
		header.add(createHeader("access_token", token));
		header.add(createHeader("client_secret", "a3f63ee0f0fc44848eeef91109396295"));
		header.add(createHeader("client_id", "MYALLIANZ"));

		JsonDetail jsonDet = new JsonDetail();
		Url url = new Url();
		jsonDet = createJsonDetail(url.urlList.get(1), "{}", header);

		return getGetRequest(name, jsonDet);
	}

	// 03-myclaimusage
	public String setHeaderClaim() {
		String name = "03-myclaimusage";
		List<Header> header = new ArrayList<Header>();
		header.add(createHeader("code", "3100500776711"));
		header.add(createHeader("Accept", "application/json, text/plain, */*"));
		header.add(createHeader("version_no", "1.1"));
		header.add(createHeader("access_token", token));
		header.add(createHeader("client_secret", "a3f63ee0f0fc44848eeef91109396295"));
		header.add(createHeader("client_id", "MYALLIANZ"));

		JsonDetail jsonDet = new JsonDetail();
		Url url = new Url();
		jsonDet = createJsonDetail(url.urlList.get(2), "", header);

		return getGetRequest(name, jsonDet);
	}

	// 04-notification
	public String setHeaderNoti() {
		String name = "04-notification";
		List<Header> header = new ArrayList<Header>();
		header.add(createHeader("code", "3100500776711"));
		header.add(createHeader("device_id", ""));
		header.add(createHeader("Content-Type", "application/json"));
		header.add(createHeader("Accept", "application/json, text/plain, */*"));
		header.add(createHeader("version_no", "1.1"));
		header.add(createHeader("access_token", token));
		header.add(createHeader("client_secret", "a3f63ee0f0fc44848eeef91109396295"));

		JsonDetail jsonDet = new JsonDetail();
		Url url = new Url();
		jsonDet = createJsonDetail(url.urlList.get(3),
				"{" + "\"agentCode\":\"3100500776711\",\"msgTopic\":\"\",\"notifyFlag\":true,"
						+ "\"fromSystem\":\"\",\"notifyDate\":\"\",\"announceType\":\"\","
						+ "\"groupID\":11,\"channel\":\"\",\"msgID\":\"\",\"messageDetails\":null,"
						+ "\"action\":\"get\"}",
				header);

		return getPostRequest(name, jsonDet);
	}

	// 05-myclaimhistory
	public String setHeaderClaimHis() {
		String name = "05-myclaimhistory";
		List<Header> header = new ArrayList<Header>();
		header.add(createHeader("code", "3100500776711"));
		header.add(createHeader("Accept", "application/json, text/plain, */*"));
		header.add(createHeader("version_no", "1.1"));
		header.add(createHeader("access_token", token));
		header.add(createHeader("client_secret", "a3f63ee0f0fc44848eeef91109396295"));
		header.add(createHeader("client_id", "MYALLIANZ"));

		JsonDetail jsonDet = new JsonDetail();
		Url url = new Url();
		jsonDet = createJsonDetail(url.urlList.get(4), "", header);

		return getGetRequest(name, jsonDet);
	}

	// azPaymentStatus
	public String setAzPaymentStatus() {
		String name = "azPaymentStatus";
		List<Header> header = new ArrayList<Header>();
		header.add(createHeader("code", "3100500776711"));
		header.add(createHeader("Origin", "http://localhost:8000"));
		header.add(createHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) "
				+ "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36"));
		header.add(createHeader("Content-Type", "application/json"));
		header.add(createHeader("Accept", "application/json, text/plain, */*"));
		header.add(createHeader("version_no", "1.1"));
		header.add(createHeader("access_token", token));
		header.add(createHeader("client_secret", "a3f63ee0f0fc44848eeef91109396295"));
		header.add(createHeader("client_id", "MYALLIANZ"));

		JsonDetail jsonDet = new JsonDetail();
		Url url = new Url();
		jsonDet = createJsonDetail(url.urlList.get(5), "[{\"ref1\":\"K100265691\"," + "\"ref2\":\"20170619\"}]",
				header);

		return getPostRequest(name, jsonDet);
	}

	private JsonDetail createJsonDetail(String http, String body, List<Header> header) {
		JsonDetail url = new JsonDetail();
		url.setUrl(http);
		url.setBody(body);
		url.setHeader(header);
		return url;
	}

	private Header createHeader(String key, String value) {
		Header json = new Header();
		json.setKey(key);
		json.setValue(value);
		return json;
	}

	public String getPostRequest(String name, JsonDetail jsonDet) {
		System.out.println("Name :" + name);
		try {
			URL url = new URL(jsonDet.getUrl());
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			if (url.getHost().length() == 0 || url.getHost() == null) {
				System.out.println("Json URL bad request");
				// System.exit(0);
				return generateResult("");
			} else {

				con.setRequestMethod("POST");
				for (Header h : jsonDet.getHeader()) {
					con.setRequestProperty(h.getKey(), h.getValue());
					// System.out.println(h.getKey()+" : "+h.getValue());
				}

				// For POST only - START
				con.setDoOutput(true);
				OutputStream os = con.getOutputStream();
				os.write(jsonDet.getBody().getBytes());
				os.flush();
				os.close();
				// For POST only - END
				if (con.getHeaderField(0) == null) {
					System.out.println("HttpURLConnection bad request");
					// System.exit(0);
					return generateResult("");
				} else {
					int responseCode = con.getResponseCode();
					System.out.println("\nSending 'POST' request to URL : " + url);
					System.out.println("Response Code : " + responseCode);

					if (responseCode == HttpURLConnection.HTTP_OK) { // success
						BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();

						if (jsonDet.getUrl().equals("http://e-toyz.azay.co.th/AZauth/api/login")) {
							while ((inputLine = in.readLine()) != null) {
								response.append(inputLine);
								if (inputLine.toString().contains("access_token")) {
									token = inputLine.toString().substring(inputLine.toString().indexOf(":") + 3,
											inputLine.toString().length() - 2);
								}
							}
							in.close();
						}
						System.out.println("token :" + token);

						// print result
						System.out.println(response.toString());
						System.out.println("-----------------------------------------------\n");
						return generateResult(Integer.toString(responseCode));
					} else {
						System.out.println("POST request not worked");
						return generateResult("");
					}
				}
			}

		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
			return generateResult(e.getMessage());
		}

	}

	public String getGetRequest(String name, JsonDetail jsonDet) {
		System.out.println("Name :" + name);
		String jUrl = jsonDet.getUrl();

		try {
			URL url = new URL(jUrl);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			if (url.getHost().length() == 0 || url.getHost() == null) {
				System.out.println("Json URL bad request");
				// System.exit(0);
				return generateResult("");
			} else {
				// optional default is GET
				con.setRequestMethod("GET");

				// add request header
				for (Header h : jsonDet.getHeader()) {
					con.setRequestProperty(h.getKey(), h.getValue());
					// System.out.println(h.getKey()+" : "+h.getValue());
				}
				System.out.println(con.getHeaderField(0));
				if (con.getHeaderField(0) == null) {
					System.out.println("HttpURLConnection bad request");
					// System.exit(0);
					return generateResult("");
				} else {
					int responseCode = con.getResponseCode();

					System.out.println("\nSending 'GET' request to URL : " + jUrl);
					System.out.println("Response Code : " + responseCode);
					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					System.out.println("-----------------------------------------------\n");
					return generateResult(Integer.toString(responseCode));
				}
			}
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
			return generateResult(e.getMessage());
		}

	}

	public String generateResult(String result) {
		if (result.equals("200")) {
			return "PASS";
		}
		return "NOT PASS";
	}

	public void addResult(String result2) {
		result.add(result2);
	}

	public List<String> getResult() {
		return result;
	}

}
